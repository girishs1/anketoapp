const { Pool } = require('pg');
const pool = new Pool();

async function appListQuery() {

    const db = await pool.connect();
    const sql = `SELECT app_number, name, email, (select count(*) FROM qbank, qapp_answers WHERE qbank.question_id = qapp_answers.question_id    
                AND qapp_answers.app_number=qapp.app_number  AND qapp_answers.selected_answer = qbank.qanswer) AS correct_answers,(select count(*) as TOTAL_ANSWERS FROM qbank, qapp_answers WHERE qbank.question_id = qapp_answers.question_id    
                AND qapp_answers.app_number=qapp.app_number) AS total_answers FROM qapp
                ORDER BY app_number DESC`

    const results = await db.query(sql);

    let appList = [];
    results.rows.forEach(element => {
        appList.push(element);
    });
    db.release();
    return appList;
}
module.exports = appListQuery;