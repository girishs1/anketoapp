const { Pool } = require('pg');
const pool = new Pool();

async function appDetailQuery(appNumber) {

    const db = await pool.connect();
    const sql = `SELECT qbank.question_id,qtext,qanswer,selected_answer,qoptions,tags, difficulty_level
                FROM qbank, qapp_answers 
                WHERE qbank.question_id = qapp_answers.question_id AND qapp_answers.app_number=$1`;
    const results = await db.query(sql,[appNumber]);

    let appDetail = [];
    results.rows.forEach(element => {
        appDetail.push(element);
    });
    db.release();
    return appDetail;
}
module.exports = appDetailQuery;