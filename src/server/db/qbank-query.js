const {Pool} = require('pg');
const pool = new Pool();

async function fetchQBank(category) {
    try {
        const db = await  pool.connect();
        const sql = `select * from qbank where category = $1 order by asc_order`;
        const results = await db.query(sql, [category]);
        let qBankList = [];
        results.rows.forEach(element => {
            qBankList.push(element);
        });
        db.release();
        return qBankList;
    }
    catch (err) {
        console.log('INCIDE CATCH : qBankQuery ', err);
        throw err;
    }
}

async function fetchCategories() {
    try {
        const db = await  pool.connect();
        const sql = `select distinct category from qbank`;
        const results = await db.query(sql);
        let catList = [];
        results.rows.forEach(element => {
        // console.log(element);
            catList.push(element.category);
        });
        db.release();
        return catList;
    }
    catch (err) {
        console.log('INCIDE CATCH : fetchCategories ', err);
        throw err;
    }
}

async function tokenList() {
    try {
        const db = await  pool.connect();
        const sql = `select * from assessment_token where used=false order by token_id desc`;
        const results = await db.query(sql);
        let tokenList = [];
        results.rows.forEach(element => {
            tokenList.push(element);
        });
        db.release();
        return tokenList;
    }
    catch (err) {
        console.log('INCIDE CATCH : tokenList ', err);
        throw err;
    }
}


async function fetchToken(tokenNumber) {
    try {
        const db = await  pool.connect();
        const sql = `select * from assessment_token where token_number=$1 and used=false order by token_id desc`;
        const results = await db.query(sql,[tokenNumber]);
        //console.log(results);
        let tokenData = results.rows[0];
        db.release();
        return tokenData;
    }
    catch (err) {
        console.log('INCIDE CATCH : fetchToken ', err);
        throw err;
    }
}
module.exports ={fetchQBank,fetchCategories,tokenList,fetchToken}; 