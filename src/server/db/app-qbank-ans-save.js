const { Pool } = require('pg')
const pool = new Pool()

async function appQBankPost(questions, appData) {
    return new Promise(function (resolve, reject) {
    
            ; (async () => {
                // note: we don't try/catch this because if connecting throws an exception
                // we don't need to dispose of the client (it will be undefined)
                const client = await pool.connect()
                try {
                    await client.query('BEGIN')
                    //console.log('appData',appData);

                    const insertAppQuery = 'INSERT INTO qapp(email,name) VALUES($1,$2) RETURNING app_number'
                    //  console.log(insertAppQuery);
                    const res = await client.query(insertAppQuery, [appData.emailId,appData.name]);
                    const appNumber = res.rows[0].app_number;

                    for (var i = 0; i < questions.length; i++) {
                        const insertAnsQuery = 'INSERT INTO qapp_answers(question_id, app_number, selected_answer) VALUES ($1, $2, $3)';
                        const insertAnsValues = [questions[i].question_id, appNumber, [questions[i].selectedAnswer]];
                        await client.query(insertAnsQuery, insertAnsValues);
                    }

                    const tokenQuery = `update assessment_token set used=true WHERE token_id=$1`;
                   // console.log(tokenQuery);
                   // console.log('TOKEN_ID:::::::::::::::::::::', appData.tokenId);

                    await client.query(tokenQuery,[appData.tokenId]);

                    await client.query('COMMIT');
                    resolve(res.rows[0]);
                } catch (e) {
                    await client.query('ROLLBACK');
                    console.log('error has occurred');
                    console.log(e);
                    throw e;
                } finally {
                    client.release()
                }
            })().catch(e => {
                console.error(e.stack);
                throw e;
            })
    }
    )
}

module.exports = appQBankPost;
