const { Pool } = require('pg')
const pool = new Pool()

async function saveToken(token) {
   
    
    return new Promise(function (resolve, reject) {

        ;(async () => {
            // note: we don't try/catch this because if connecting throws an exception
            // we don't need to dispose of the client (it will be undefined)
            const client = await pool.connect()
            try {
                const insertTokenQuery = `INSERT INTO assessment_token(
                    token_number, name, email, qbank_category, no_of_questions, difficulty_level)  VALUES ( $1, $2, $3, $4, $5, $6) returning  token_id,name`;

                const res = await client.query(insertTokenQuery, [token.tokenNumber, token.name, token.email, token.qbankCategory,
                token.noOfQuestions, token.difficultyLevel]);
                let obj = {tokenNumber:token.tokenNumber, tokenId: res.rows[0].token_id, name: res.rows[0].name};
                resolve(obj);
            } catch (e) {
                console.log('error has occurred');
                console.log(e);
                throw e;
            } finally {
                client.release()
            }
        })().catch(e => {
            console.error(e.stack);
            throw e;
        })

    })
}

module.exports = saveToken;