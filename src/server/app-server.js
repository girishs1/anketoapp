const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");

const qBankRouter = require("./routes/qbank-router");
const appRouter = require("./routes/app-router");

const PORT = process.env.PORT || 7200;

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
app.use(cors());

// To make react app work in prod
if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "../../build")));
  app.get("/react/*", function(req, res) {
    res.sendFile(path.join(__dirname, "../../build", "index.html"));
  });
}

app.listen(PORT, () => {
  console.log(`Server listening to port ${PORT}`);
});

app.use("/qbank", qBankRouter);
app.use("/manager", appRouter);
