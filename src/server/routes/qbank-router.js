const express = require('express');
var router = express.Router([{ mergeParams: true }]);

router.get('/fetchQ/:category', (req, res) => {
    const dbFunctions = require('../db/qbank-query');
    // Problem with http time out for Async calls. Fix this problem so that ReactJS responds immediatly for system problems 
    const category = req.params.category;
    (async function query() {
        let qBankList = await dbFunctions.fetchQBank(category);
        let qList = { questions: qBankList };
        res.end(JSON.stringify(qList));
        
    })();
});


router.get('/fetchCategories', (req, res) => {
    const dbFunctions = require('../db/qbank-query');
    // Problem with http time out for Async calls. Fix this problem so that ReactJS responds immediatly for system problems 
    (async function query() {
        let catList = await dbFunctions.fetchCategories();
        let cList = { categories: catList };
        res.end(JSON.stringify(cList));
        
    })();
});

router.post('/submitAnswers', (req, res) => {
    const saveAnswersDb = require('../db/app-qbank-ans-save');
    let app = {};
   /* const { questions, emailId, name , tokenId} = req.body;
    app.emailId = emailId;
    app.name = name;
    app.tokenId = tokenId;*/
   // console.log('req.body', req.body);
    let response = saveAnswersDb(req.body.questions, req.body);
    response.then(data => {
        //console.log('after insert data::' + JSON.stringify(data.app_number))
        // return success
        res.end(JSON.stringify(data.app_number));
    }
    );
})

router.post('/generateToken',(req,res) => {

    const saveToken = require('../db/token-save');
    let token = req.body;

    token.tokenNumber = Math.trunc(Math.random()*10000);
   
    let response = saveToken(token);

    response.then(data => {
        res.end(JSON.stringify(data));
    }
    );

});

router.get('/fetchTokenList',(req,res) => {
    const dbFunctions = require('../db/qbank-query');
    let response = dbFunctions.tokenList();

    response.then(data => {
        //console.log('jSon data from fetchTokenList', data);
        res.end(JSON.stringify({tokenList : data}));
    }
    );
});

router.get('/fetchTokenDetail/:tokenNumber',(req,res) => {
    const dbFunctions = require('../db/qbank-query');
    const tokenNumber = req.params.tokenNumber;
    let response = dbFunctions.fetchToken(tokenNumber);

    //console.log('After calling fetchToken method');

    response.then(data => {
       // console.log('jSon data from fetchTokenDetail', data);
        res.end(JSON.stringify({tokenDetail : data}));
    }
    );
});

module.exports = router;