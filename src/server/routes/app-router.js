const express = require('express');
var router = express.Router([{ mergeParams: true }]);

router.get('/app/list', (req, res) => {
    const appListQuery = require('../db/app-list-query');
    (async function query() {
        let results = await appListQuery();
        let appList = { appList:results };
        res.end(JSON.stringify(appList));
    })();
});


router.get('/app/detail/:appNumber', (req, res) => {
    const appDetailQuery = require('../db/app-detail-query');
    const appNumber = req.params.appNumber;
    (async function query() {
        let results = await appDetailQuery(appNumber);
        let appDetail = { appDetail:results };
        res.end(JSON.stringify(appDetail));
    })();
});

module.exports = router;