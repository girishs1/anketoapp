import React from 'react';

// ES6
import ReactTable from 'react-table';
import 'react-table/react-table.css';


//import { Link } from "react-router-dom";

export class TokenList extends React.Component {

  constructor(props) {
    super(props);

    //temp solution to make it work in wifi
    let host = window.location.hostname;

    this.state = { isLoaded: false, host };
  }

  componentDidMount() {
    const URL = (process.env.NODE_ENV === 'production' ? `/qbank/fetchTokenList` : `http://${this.state.host}:7200/qbank/fetchTokenList`);
    //alert(URL);
    fetch(URL)
      .then(response => response.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            isError: false,
            tokenList: result.tokenList
          });
          
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            isError: true,
            error
          });
        }
      )
  }

  render() {

    if (this.state.isError) // wait for async calls to finish in beginning of page
      throw this.state.error;

    if (!this.state.isLoaded) // wait for async calls to finish in beginning of page
      return null;


      //{"token_id":90,"token_number":9267,"name":"aish","email":"aish@COM","qbank_category":"type1-java-aptitude","no_of_questions":20,"difficulty_level":null,"used":false},
    const columns = [{
      Header: 'Token Number',
      accessor: 'token_number',
      width: 120,
      headerStyle: {
        background: 'auto',
        textAlign: 'left',
        color: 'auto',
        fontSize: '110%',
        fontWeight: '700'
      },
      style: {
         fontSize: '110%',
         color : 'blue',
         textAlign: 'center'
      }
      }, {
      Header: 'Name',
      accessor: 'name',
      width: 220,
      headerStyle: {
        background: 'auto',
        textAlign: 'left',
        color: 'auto',
        fontSize: '110%',
        fontWeight: '700'
      }, style: {
        fontSize: '110%'
      }
    }, {
        Header: 'Email',
        accessor: 'email',
        width: 220,
        headerStyle: {
          background: 'auto',
          textAlign: 'left',
          color: 'auto',
          fontSize: '110%',
          fontWeight: '700'
        },
        style: {
          fontSize: '110%'
        }
      }, {
        Header: 'qBank Type',
        accessor: 'qbank_category',
        width: 220,
        headerStyle: {
          background: 'auto',
          textAlign: 'left',
          color: 'auto',
          fontSize: '110%',
          fontWeight: '700'
        }, style: {
          fontSize: '110%'
        }
      },{
      Header: 'Total Questions',
      accessor: 'no_of_questions',
      width: 160,
      headerStyle: {
        background: 'auto',
        textAlign: 'left',
        color: 'auto',
        fontSize: '110%',
        fontWeight: '700'
      },
      style: {
        textAlign: 'center',
        fontSize: '110%'
      }
    }
    ];

    const divStyle = { width: '90%' };

    return <div><h1>Generated Token List</h1> <br />
      <div style={divStyle}><ReactTable
        data={this.state.tokenList}
        columns={columns}
      />
      </div>
    </div>

  }
}