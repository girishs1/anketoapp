import React from "react";

export class AppDetail extends React.Component {
  constructor(props) {
    super(props);
    let host = window.location.hostname;
    this.state = { isLoaded: false, host };
    this.goBack = this.goBack.bind(this);
  }
  componentDidMount() {
    let { appNumber } = this.props.match.params;
    const URL =
      process.env.NODE_ENV === "production"
        ? `/manager/app/detail/${appNumber}`
        : `http://${this.state.host}:7200/manager/app/detail/${appNumber}`;
    fetch(URL)
      .then(response => response.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            appDetail: result.appDetail,
            appNumber: appNumber
          });
          //  console.log('app results from react' + result.appList[0].app_number);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  goBack() {
    this.props.history.push("/Manager/QBankResult/App/Summary");
  }

  render() {
    if (!this.state.isLoaded)
      // wait for async calls to finish in beginning of page
      return null;

    console.log(this.state.appDetail);

    const liStyle = {
      listStyleType: "upper-roman",
      width: "20%"
    };
    const appDetail = (
      <div>
        {" "}
        <h1>Assessment Result for Application # {this.state.appNumber}</h1>
      </div>
    );
    return (
      <div>
        {appDetail}
        <br />
        <hr style={{ borderTop: "2px solid blue" }} />
        {this.state.appDetail.map((question, index) => (
          <div style={{ width: "90%" }}>
            <h5>
              {index + 1} .
              {question.qtext.split("\n").map((item, key) => {
                return (
                  <span style={{ color: "#5D6D7E" }} key={key}>
                    {item} <br />
                  </span>
                );
              })}
            </h5>
            <br />
            <ol>
              {question.qoptions.map(option => (
                <li
                  style={
                    (liStyle,
                    question.selected_answer.findIndex(
                      element => element === option
                    ) > -1
                      ? { backgroundColor: "#839192" }
                      : { backgroundColor: "auto" })
                  }
                >
                  {option}{" "}
                  {question.qanswer.findIndex(element => element === option) >
                  -1 ? (
                    <span>&#10003;</span>
                  ) : (
                    ""
                  )}
                </li>
              ))}
            </ol>
            <hr style={{ borderTop: "4px dotted blue" }} />
          </div>
        ))}

        <br />
        <br />

        <div>
          <button className="btn btn-primary" onClick={this.goBack}>
            Go Back
          </button>
        </div>
      </div>
    );
  }
}
