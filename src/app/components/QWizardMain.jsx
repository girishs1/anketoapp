import React from "react";
import { QConfirmation } from "./QWizardConfirmation";

export class QWizardMain extends React.Component {
  constructor(props) {
    super(props);

    //temp solution to make it work in wifi
    let host = window.location.hostname;
    this.state = {
      qIndex: 0,
      isLoaded: false,
      confirmWizard: false,
      isSubmitted: false,
      host,
      name: props.name,
      email: props.email,
      category: props.category,
      tokenId: props.tokenId
    };

    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.handleOptions = this.handleOptions.bind(this);
    this.confirm = this.confirm.bind(this);
    this.submitAnswers = this.submitAnswers.bind(this);
    this.setQWizard = this.setQWizard.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleName = this.handleName.bind(this);
  }
  componentDidMount() {
    let URL =
      process.env.NODE_ENV === "production"
        ? "/qbank/fetchq/"
        : `http://${this.state.host}:7200/qbank/fetchq/`;
    URL += this.state.category;
    fetch(URL)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            isError: false,
            questionList: result
          });
          console.log("results from react" + result.questions.length);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isError: true,
            error
          });
          //alert('error');
          // alert(error);
          console.log("Application error has occured....");
          alert("incide error state", error.message);
          console.error(error);
          throw error;
        }
      );
    /*     .catch(function (error) {
                alert('Fetch Request failed:', error.message);
                console.log('Fetch Request failed');
                console.error(error);
                throw error;
            });*/
  }

  handlePrev(event) {
    this.setState(state => ({
      qIndex: state.qIndex - 1
    }));
  }

  handleNext(event) {
    this.setState(state => ({
      qIndex: state.qIndex + 1
    }));
  }

  handleOptions(event) {
    const selectedAnswer = event.target.value;
    this.setState(state => {
      state.questionList.questions[
        this.state.qIndex
      ].selectedAnswer = selectedAnswer;
      return state;
    });
  }

  handleEmail(email) {
    this.setState(state => {
      state.email = email;
      return state;
    });
  }

  handleName(name) {
    this.setState(state => {
      state.name = name;
      return state;
    });
  }

  submitAnswers() {
    const URL =
      process.env.NODE_ENV === "production"
        ? "/qbank/submitAnswers"
        : `http://${this.state.host}:7200/qbank/submitAnswers`;
    let data = this.state.questionList;
    data.emailId = this.state.email;
    data.name = this.state.name;
    data.tokenId = this.state.tokenId;
    (async parentObject => {
      try {
        const response = fetch(URL, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, *cors, same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          credentials: "same-origin", // include, *same-origin, omit
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        response.then(function(body) {
          console.log(
            "GOT response.........finally.",
            body.json().then(data => {
              console.log(data);
              parentObject.setState(state => {
                state.isSubmitted = true;
                state.applicationNo = data;
                return state;
              });
            })
          );
        });
      } catch (error) {
        console.log(error);
      }
    })(this);
  }

  setQWizard() {
    this.setState(state => {
      state.confirmWizard = false;
      return state;
    });
    //this.forceUpdate();
  }

  confirm(event) {
    if (window.confirm("Are you sure you want to submit answers?")) {
      this.setState(state => {
        state.confirmWizard = true;
        return state;
      });
    }
  }

  render() {
    if (this.state.isError)
      // wait for async calls to finish in beginning of page
      throw this.state.error;

    if (!this.state.isLoaded)
      // wait for async calls to finish in beginning of page
      return null;

    let nextStep =
      this.state.qIndex < this.state.questionList.questions.length - 1
        ? true
        : false;
    let prevStep = this.state.qIndex > 0 ? true : false;

    if (this.state.isSubmitted === true) {
      return (
        <div>
          <h1>QWizard</h1>
          <br />
          <div className="display-success">
            Application # {this.state.applicationNo} is successfully submitted
            for Evaluation !!!
          </div>
        </div>
      );
    }

    if (this.state.confirmWizard === true) {
      return (
        <QConfirmation
          email={this.state.email}
          name={this.state.name}
          submitAnswers={this.submitAnswers}
          setQWizard={this.setQWizard}
          handleEmail={this.handleEmail}
          handleName={this.handleName}
        />
      );
    }

    const spacing = { margin: "10px" };

    return (
      <div>
        <h1>
          QWizard - {this.state.qIndex + 1} of{" "}
          {this.state.questionList.questions.length}
        </h1>
        <br />
        <div>
          <h4>
            {this.state.questionList.questions[this.state.qIndex].qtext
              .split("\n")
              .map((item, key) => {
                return (
                  <span style={{ color: "#5D6D7E" }} key={key}>
                    {item} <br />
                  </span>
                );
              })}
          </h4>

          <br />
          {this.state.questionList.questions[this.state.qIndex].qoptions.map(
            option => (
              <div class="h4">
                <input
                  type="radio"
                  key={
                    this.state.questionList.questions[this.state.qIndex]
                      .question_id
                  }
                  name={
                    this.state.questionList.questions[this.state.qIndex]
                      .question_id
                  }
                  value={option}
                  checked={
                    option ===
                    this.state.questionList.questions[this.state.qIndex]
                      .selectedAnswer
                      ? true
                      : false
                  }
                  onChange={this.handleOptions}
                />
                <span> {option}</span>
              </div>
            )
          )}
          <div>
            <br />
            <input
              type="button"
              style={spacing}
              className="btn btn-primary"
              disabled={!prevStep}
              name="prev"
              value="prev"
              onClick={this.handlePrev}
            />
            <input
              type="button"
              style={spacing}
              className="btn btn-primary"
              disabled={!nextStep}
              name="next"
              value="next"
              onClick={this.handleNext}
            />
            {nextStep === false ? (
              <input
                type="button"
                style={spacing}
                className="btn btn-primary"
                name="submit"
                value="submit answers"
                onClick={this.confirm}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    );
  }
}
