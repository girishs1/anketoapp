import React from 'react';


export class QConfirmation extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = { confirmation: true };
        this.goBack = this.goBack.bind(this);
        this.submitAnswers = this.submitAnswers.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleName = this.handleName.bind(this);

    }

    handleEmail(event) {
        const email = event.target.value;
        this.props.handleEmail(email);
       
    }

    handleName(event) {
        const name = event.target.value;
        this.props.handleName(name);
  
    }

    goBack(event) {
        this.props.setQWizard();
    }

    submitAnswers(event) {
        this.props.submitAnswers();
    }

    render() {
        const spacing = {margin:"10px" };

        return <div>
            <div>
                <label>Email::
                    <input type="text" style={spacing} name="emailId" value={this.props.email} onChange={this.handleEmail} />
                </label>
            </div>
            <div>
                <label>Name:
                    <input type="text" style={spacing} name="name" value={this.props.name} onChange={this.handleName} />
                </label>
            </div>

            <div>
                <input type="button" style={spacing} className="btn btn-primary" name="GoBack" value="go back" onClick={this.goBack} />
                <input type="button" style={spacing} className="btn btn-primary" name="confirm" value="confirm submission" onClick={this.submitAnswers} />
            </div>
        </div>
    }
}