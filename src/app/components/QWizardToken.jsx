import React from 'react';
import Form from 'react-bootstrap/Form';
import { QWizardMain } from './QWizardMain';


export class QWizardToken extends React.Component {

    constructor(props) {
        super(props);

        //temp solution to make it work in wifi
        let host = window.location.hostname;

        this.state = { tokenNumber: "", startAssessment: false, host };

        this.reset = this.reset.bind(this);
        this.pullAssessment = this.pullAssessment.bind(this);
        this.handleTokenNumber = this.handleTokenNumber.bind(this);

    }

    handleTokenNumber(event) {
        const _tokenNumber = event.target.value;
        this.setState((state) => { state.tokenNumber = _tokenNumber; return state });

    }
    pullAssessment(event) {

        event.preventDefault();

        if (this.state.tokenNumber.length !== 4 ) {
            alert("Please enter correct 4-digit Token");
            return;
        }

        let URL = (process.env.NODE_ENV === 'production' ? '/qbank/fetchTokenDetail/' : `http://${this.state.host}:7200/qbank/fetchTokenDetail/`);
        URL += this.state.tokenNumber;
        //alert(URL);
    
        (async (parentObject) => {
            try {
                const response = fetch(URL, {
                    method: 'GET', // *GET, POST, PUT, DELETE, etc.
                    mode: 'cors', // no-cors, *cors, same-origin
                    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                    credentials: 'same-origin', // include, *same-origin, omit
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    redirect: 'follow', // manual, *follow, error
                    referrer: 'no-referrer', // no-referrer, *client
                });

                //console.log('Before calling promise object');

                response.then(function (body) {
                    console.log("GOT QBank response", body.json().then(data => {
                        //console.log('RESPONSE',data);
                        //console.log('RESPONSE length:',Object.entries(data).length);
                        // check if object is empty    
                        if (Object.entries(data).length === 0) {
                            alert("Please enter correct 4-Digit Token provided by Hiring Manager");

                            parentObject.setState((state=> {
                                state.startAssessment = false;
                                return state;
                            }))
                            return;
                        }

                        parentObject.setState(((state) => {
                            //   //{"tokenDetail":{"token_id":90,"token_number":9267,"name":"aish","email":"aish@COM","qbank_category":"type1-java-aptitude","no_of_questions":20,"difficulty_level":null,"used":false}}
                            state.tokenId = data.tokenDetail.token_id;
                            state.tokenNumber = data.tokenDetail.token_number;
                            state.name = data.tokenDetail.name;
                            state.email = data.tokenDetail.email;
                            state.qBankCategory = data.tokenDetail.qbank_category;
                            state.totalQuestions = data.tokenDetail.no_of_questions;

                            state.startAssessment = true;
                            return state;
                        }));

                    }));
                })
            }
            catch (error) {
                console.log(error);
            }
        })(this);

    }

    reset(e) {
        this.setState({ tokenNumber: "" });
    }

    render() {
        const spacing = { margin: "10px" };

        if (this.state.startAssessment)
            return <QWizardMain name={this.state.name} email={this.state.email} category={this.state.qBankCategory} tokenId={this.state.tokenId} />

        return (
            <Form>
                <Form.Group controlId="tokenId">
                    <Form.Label>Token Number</Form.Label>

                    <Form.Control id="tokenNumber" name="tokenNumber" type="number" placeholder="" onChange={this.handleTokenNumber} style={{ width: '50%' }} />
                    <Form.Text className="text-muted" >
                      TO GET STARTED, PLEASE ENTER TOKEN ID (4-Digits) PROVIDED BY HIRING MANAGER
                     </Form.Text>
                    <br />
                    <br />
                    <button style={spacing} className="btn btn-primary" onClick={this.pullAssessment}>start assessment</button>
                    <button style={spacing} className="btn btn-primary" onClick={this.reset}>reset</button>

                </Form.Group>
            </Form>
        );
    }


}