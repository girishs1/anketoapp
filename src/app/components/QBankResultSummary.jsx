import React from "react";

// ES6
import ReactTable from "react-table";
import "react-table/react-table.css";

import { Link } from "react-router-dom";

export class QBankResultSummary extends React.Component {
  constructor(props) {
    super(props);

    //temp solution to make it work in wifi
    let host = window.location.hostname;

    this.state = { isLoaded: false, host };
  }

  componentDidMount() {
    const URL =
      process.env.NODE_ENV === "production"
        ? "/manager/app/list"
        : `http://${this.state.host}:7200/manager/app/list`;
    fetch(URL)
      .then(response => response.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            isError: false,
            appList: result.appList
          });
          //  console.log('app results from react' + result.appList[0].app_number);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            isError: true,
            error
          });
        }
      );
  }

  render() {
    if (this.state.isError)
      // wait for async calls to finish in beginning of page
      throw this.state.error;

    if (!this.state.isLoaded)
      // wait for async calls to finish in beginning of page
      return null;

    const columns = [
      {
        Header: "App Number",
        accessor: "app_number",
        width: 120,
        Cell: props => (
          <Link
            style={{ color: "blue" }}
            to={{
              pathname: `/react/Manager/QBankResult/App/Detail/${props.value}`
            }}
          >
            {props.value}{" "}
          </Link>
        ),
        headerStyle: {
          background: "auto",
          textAlign: "left",
          color: "auto",
          fontSize: "110%",
          fontWeight: "700"
        },
        style: {
          fontSize: "110%"
        }
      },
      {
        Header: "Email",
        accessor: "email",
        width: 220,
        headerStyle: {
          background: "auto",
          textAlign: "left",
          color: "auto",
          fontSize: "110%",
          fontWeight: "700"
        },
        style: {
          fontSize: "110%"
        }
      },
      {
        Header: "Name",
        accessor: "name",
        width: 220,
        headerStyle: {
          background: "auto",
          textAlign: "left",
          color: "auto",
          fontSize: "110%",
          fontWeight: "700"
        },
        style: {
          fontSize: "110%"
        }
      },
      {
        Header: "Correct Answers",
        accessor: "correct_answers",
        width: 160,
        headerStyle: {
          background: "auto",
          textAlign: "left",
          color: "auto",
          fontSize: "110%",
          fontWeight: "700"
        },
        style: {
          textAlign: "center",
          fontSize: "110%"
        }
      },
      {
        Header: "Total Questions",
        accessor: "total_answers",
        width: 160,
        headerStyle: {
          background: "auto",
          textAlign: "left",
          color: "auto",
          fontSize: "110%",
          fontWeight: "700"
        },
        style: {
          textAlign: "center",
          fontSize: "110%"
        }
      }
    ];

    const divStyle = { width: "90%" };

    return (
      <div>
        <h1>Assessment Results</h1> <br />
        <div style={divStyle}>
          <ReactTable data={this.state.appList} columns={columns} />
        </div>
      </div>
    );
  }
}
