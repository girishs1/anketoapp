import React from "react";
import Form from "react-bootstrap/Form";

export class TokenForm extends React.Component {
  constructor(props) {
    super(props);

    //temp solution to make it work in wifi
    let host = window.location.hostname;

    this.state = {
      isLoaded: false,
      email: "",
      name: "",
      noOfQuestions: 20,
      host
    };

    this.handleEmail = this.handleEmail.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleCategory = this.handleCategory.bind(this);
    this.handleNoOfQuestions = this.handleNoOfQuestions.bind(this);

    this.generate = this.generate.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  handleName(event) {
    const name = event.target.value;
    this.setState(state => {
      state.name = name;
      return state;
    });
  }

  handleEmail(event) {
    const email = event.target.value;
    this.setState(state => {
      state.email = email;
      return state;
    });
  }

  handleCategory(event) {
    const cat = event.target.value;
    this.setState(state => {
      state.category = cat;
      return state;
    });
  }

  handleNoOfQuestions(event) {
    const qNos = event.target.value;
    this.setState(state => {
      state.noOfQuestions = qNos;
      return state;
    });
  }

  goBack() {
    this.props.history.push("/");
  }

  generate(e) {
    e.preventDefault();
    const URL =
      process.env.NODE_ENV === "production"
        ? `/qbank/generateToken`
        : `http://${this.state.host}:7200/qbank/generateToken`;
    let data = {};
    data.name = this.state.name;
    data.email = this.state.email;
    data.qbankCategory = this.state.category;
    data.noOfQuestions = this.state.noOfQuestions;
    (async parentObject => {
      try {
        const response = fetch(URL, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, *cors, same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          credentials: "same-origin", // include, *same-origin, omit
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow", // manual, *follow, error
          referrer: "no-referrer", // no-referrer, *client
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        response.then(function(body) {
          console.log(
            "GOT response.........finally.",
            body.json().then(data => {
              console.log(data);
              parentObject.setState(state => {
                state.tokenNumber = data.tokenNumber;
                let message = `New token ${data.tokenNumber} generated successfully`;
                parentObject.setSuccessMessage(message);
                //window.alert(message);
                state.email = "";
                state.name = "";
                state.noOfQuestions = 20;
                //parentObject.forceUpdate();
                return state;
              });
            })
          );
        });
      } catch (error) {
        console.log(error);
      }
    })(this);
  }

  setSuccessMessage(message) {
    this.setState({
      successMessage: message
    });
    setTimeout(() => {
      this.setState({
        successMessage: ""
      });
    }, 5000);
  }

  componentDidMount() {
    const URL =
      process.env.NODE_ENV === "production"
        ? "/qbank/fetchcategories"
        : `http://${this.state.host}:7200/qbank/fetchcategories`;
    fetch(URL)
      .then(res => res.json())
      .then(
        result => {
          let defaultCat =
            result.categories.length > 0 ? result.categories[0] : "";
          this.setState({
            isLoaded: true,
            isError: false,
            catList: result,
            category: defaultCat
          });
          console.log("react results : category:", result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isError: true,
            error
          });
          console.log("Application error has occured....");
          alert("incide error state", error.message);
          console.error(error);
          throw error;
        }
      );
  }

  render() {
    if (this.state.isError)
      // wait for async calls to finish in beginning of page
      throw this.state.error;

    if (!this.state.isLoaded)
      // wait for async calls to finish in beginning of page
      return null;

    const fmStyle = { width: "40%" };
    const spacing = { margin: "10px" };

    return (
      <div>
        <h1>Generate Assessment Token</h1>
        <br />
        <div>
          {this.state.successMessage ? (
            <div className="display-success"> {this.state.successMessage}</div>
          ) : (
            ""
          )}
          <Form style={fmStyle}>
            <Form.Group controlId="tokenForm.ControlInput1">
              <Form.Label>Candidate Name</Form.Label>
              <Form.Control
                name="name"
                value={this.state.name}
                placeholder=""
                onChange={this.handleName}
              />
            </Form.Group>
            <Form.Group controlId="tokenForm.ControlInput2">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                name="email"
                value={this.state.email}
                placeholder="name@gmail.com"
                onChange={this.handleEmail}
              />
            </Form.Group>
            <Form.Group controlId="tokenForm.ControlSelect1">
              <Form.Label>Category</Form.Label>
              <Form.Control
                as="select"
                value={this.state.category}
                onChange={this.handleCategory}
              >
                {this.state.catList.categories.map(element => (
                  <option>{element} </option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="tokenForm.ControlInput3">
              <Form.Label>Number of Questions</Form.Label>
              <Form.Control
                type="noOfQuestions"
                placeholder="20"
                value={this.props.noOfQuestions}
                onChange={this.handleNoOfQuestions}
              />
            </Form.Group>

            <button
              style={spacing}
              className="btn btn-primary"
              onClick={this.generate}
            >
              generate
            </button>
            <button
              style={spacing}
              className="btn btn-primary"
              onClick={this.goBack}
            >
              cancel
            </button>
          </Form>
        </div>
      </div>
    );
  }
}
