import React from "react";

import { TokenForm } from "./TokenForm";
import { TokenList } from "./TokenList";
import { QBankResultSummary } from "./QBankResultSummary";
import { QWizardToken } from "./QWizardToken";
import { AppDetail } from "./AppDetail";
import { ErrorBoundary } from "../utils/ErrorBoundary";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

export class Main extends React.Component {
  render() {
    return (
      <div>
        <ErrorBoundary>
          <Router>
            <Switch>
              <Route path="/react/QBank/Assess/">
                <QWizardToken />
              </Route>
              <Route path="/react/QBank/GenerateToken">
                <TokenForm />
              </Route>
              <Route path="/react/QBank/TokenList/">
                <TokenList />
              </Route>
              <Route path="/react/Manager/QBankResult/App/Summary">
                <QBankResultSummary />
              </Route>
              <Route
                path="/react/Manager/QBankResult/App/Detail/:appNumber"
                component={AppDetail}
              />

              <Route path="/*">
                <img alt="home page" src="/images/assessment.jpg" />
              </Route>
            </Switch>
          </Router>
        </ErrorBoundary>
      </div>
    );
  }
}
