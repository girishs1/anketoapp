--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0
-- Dumped by pg_dump version 12.0

-- Started on 2019-11-26 14:48:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE anketo;
--
-- TOC entry 2842 (class 1262 OID 17045)
-- Name: anketo; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE anketo WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';


ALTER DATABASE anketo OWNER TO postgres;

\connect anketo

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2843 (class 0 OID 0)
-- Dependencies: 2842
-- Name: DATABASE anketo; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE anketo IS 'online wizard app';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 17059)
-- Name: qapp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qapp (
    comments character varying,
    email text,
    app_number integer NOT NULL,
    name text
);


ALTER TABLE public.qapp OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17068)
-- Name: qapp_answers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qapp_answers (
    question_id numeric NOT NULL,
    app_number numeric NOT NULL,
    selected_answer character varying[]
);


ALTER TABLE public.qapp_answers OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 17074)
-- Name: qapp_app_number_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.qapp_app_number_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qapp_app_number_seq OWNER TO postgres;

--
-- TOC entry 2844 (class 0 OID 0)
-- Dependencies: 206
-- Name: qapp_app_number_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.qapp_app_number_seq OWNED BY public.qapp.app_number;


--
-- TOC entry 203 (class 1259 OID 17048)
-- Name: qbank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qbank (
    question_id integer NOT NULL,
    qtext character varying NOT NULL,
    qanswer character varying[],
    qoptions character varying[],
    tags character varying[],
    difficulty_level numeric,
    "order" integer
);


ALTER TABLE public.qbank OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17046)
-- Name: qbank_record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.qbank_record_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qbank_record_id_seq OWNER TO postgres;

--
-- TOC entry 2845 (class 0 OID 0)
-- Dependencies: 202
-- Name: qbank_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.qbank_record_id_seq OWNED BY public.qbank.question_id;


--
-- TOC entry 2701 (class 2604 OID 17076)
-- Name: qapp app_number; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qapp ALTER COLUMN app_number SET DEFAULT nextval('public.qapp_app_number_seq'::regclass);


--
-- TOC entry 2700 (class 2604 OID 17051)
-- Name: qbank question_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qbank ALTER COLUMN question_id SET DEFAULT nextval('public.qbank_record_id_seq'::regclass);


--
-- TOC entry 2834 (class 0 OID 17059)
-- Dependencies: 204
-- Data for Name: qapp; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.qapp (comments, email, app_number, name) VALUES (NULL, 'mayur@gmail.com', 52, 'mayur girish');
INSERT INTO public.qapp (comments, email, app_number, name) VALUES (NULL, 'jahnavi@gmail.com', 53, 'jahnavi');
INSERT INTO public.qapp (comments, email, app_number, name) VALUES (NULL, 'guru@gmail.com', 54, 'guru');


--
-- TOC entry 2835 (class 0 OID 17068)
-- Dependencies: 205
-- Data for Name: qapp_answers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (1, 52, '{11pm}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (2, 52, '{511}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (4, 52, '{"fitness walking is a convenient and valuable form of exercise."}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (3, 52, '{"20 days"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (5, 52, '{"360 160 200"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (1, 53, '{11pm}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (2, 53, '{481}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (4, 53, '{"fitness walking is a convenient and valuable form of exercise."}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (3, 53, '{"25 days"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (5, 53, '{"360 160 200"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (1, 54, '{10.30pm}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (2, 54, '{391}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (4, 54, '{"fitness walking is a better form of exercise than weight lifting"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (3, 54, '{"25 days"}');
INSERT INTO public.qapp_answers (question_id, app_number, selected_answer) VALUES (5, 54, '{"360 160 200"}');


--
-- TOC entry 2833 (class 0 OID 17048)
-- Dependencies: 203
-- Data for Name: qbank; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.qbank (question_id, qtext, qanswer, qoptions, tags, difficulty_level, "order") VALUES (1, 'A clock is set right at 5am the clock loses 16min in 24 hours. What will be the right time when the clock indicates 10am? On the 4th day?', '{11pm}', '{10.30pm,11pm,10.45pm,11.15pm}', '{type1}', 1, NULL);
INSERT INTO public.qbank (question_id, qtext, qanswer, qoptions, tags, difficulty_level, "order") VALUES (2, 'Insert the missing number. 7, 26, 63, 124, 215, 342, (....)', '{511}', '{391,421,481,511}', '{type1}', 1, NULL);
INSERT INTO public.qbank (question_id, qtext, qanswer, qoptions, tags, difficulty_level, "order") VALUES (4, 'If you’re a fitness walker, there is no need for a commute to a health club. Your neighborhood can be your health club. You don’t need a lot of fancy equipment to get a good workout either. All you need is a well-designed pair of athletic shoes.
This paragraph best supports the statement that?', '{"fitness walking is a convenient and valuable form of exercise."}', '{"fitness walking is a better form of exercise than weight lifting","a membership in a health club is a poor investment","walking outdoors provides a better workout than walking indoors. ","fitness walking is a convenient and valuable form of exercise."}', '{type1}', 1, NULL);
INSERT INTO public.qbank (question_id, qtext, qanswer, qoptions, tags, difficulty_level, "order") VALUES (3, 'If 5 people undertook a piece of construction work and finished half the job in 15 days. If two people drop out, then the job will be completed in?', '{"25 days"}', '{"25 days","20 days","15 days","10 days"}', '{type1}', 1, NULL);
INSERT INTO public.qbank (question_id, qtext, qanswer, qoptions, tags, difficulty_level, "order") VALUES (5, 'A bag contains 50 P, 25 P and 10 P coins in the ratio 5: 9: 4, amounting to Rs. 206. Find the number of coins of each type respectively', '{"200 360 160"}', '{"360 160 200","160 360 200","200 360 160","200 160 300"}', '{type1}', 1, NULL);


--
-- TOC entry 2846 (class 0 OID 0)
-- Dependencies: 206
-- Name: qapp_app_number_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.qapp_app_number_seq', 54, true);


--
-- TOC entry 2847 (class 0 OID 0)
-- Dependencies: 202
-- Name: qbank_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.qbank_record_id_seq', 5, true);


--
-- TOC entry 2705 (class 2606 OID 17102)
-- Name: qapp_answers qapp_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qapp_answers
    ADD CONSTRAINT qapp_answers_pkey PRIMARY KEY (question_id, app_number);


--
-- TOC entry 2703 (class 2606 OID 17056)
-- Name: qbank qbank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qbank
    ADD CONSTRAINT qbank_pkey PRIMARY KEY (question_id);


-- Completed on 2019-11-26 14:48:01

--
-- PostgreSQL database dump complete
--

